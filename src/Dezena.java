import java.util.HashMap;
import java.util.Map;

public class Dezena extends Extenso {

    private Extenso ex = new Unidade();
    private Map<String, String> map = new HashMap<String, String>();

    {
        map.put("1", "dez");

        map.put("11", "onze");
        map.put("12", "doze");
        map.put("13", "treze");
        map.put("14", "quatorze");
        map.put("15", "quinze");
        map.put("16", "dezesseis");
        map.put("17", "dezessete");
        map.put("18", "dezoito");
        map.put("19", "dezenove");

        map.put("2", "vinte");
        map.put("3", "trinta");
        map.put("4", "quarenta");
        map.put("5", "cinquenta");
        map.put("6", "sessenta");
        map.put("7", "setenta");
        map.put("8", "oitenta");
        map.put("9", "noventa");
    }

    public String avalia(String numero) {
        String resposta = null;

        // verifica se o número possui pelo menos 2 casas
        if (numero.length() < 2) {
            // se o numero possuir menos de duas casas passa para unidade avaliar.
            resposta = ex.avalia(numero);
        } else {

            // pega os dois digitos a esquerda do número passado: Ex: 20 > (20)
            String numeroComDoisDigitos = this.left(numero, 2);

            // se nao houver uma entrada correspondente no map ao numero de dois digitos
            if ((resposta = this.getFromMap(map, numeroComDoisDigitos)).equals("")) {

                // só com um digito Ex: 35 >(3)5
                String primeiroDigito = this.left(numero, 1);
                resposta = this.getFromMap(map, primeiroDigito);

                // Pega a string remanescente. EX: 23 --> 2(3)
                String remanescente = this.right(numero, 1);

                String unidade = ex.avalia(remanescente);
                if (unidade.equals("") || resposta.equals("")) {
                    resposta += unidade;
                } else {
                    resposta += " e " + unidade;
                }
            }
        }
        return resposta;
    }

}
