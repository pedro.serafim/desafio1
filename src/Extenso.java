import java.util.Map;

public abstract class Extenso {

    public abstract String avalia(String numero);

    public static void main(String[] args) {

        Extenso ex = new Extenso() {
            public String avalia(String numero) {
                return null;
            }
        };
    }

    protected String right(String word, Integer contar) {
        // right 1 = 123(4)
        return word.substring(word.length() - contar);
    }

    protected String left(String word, Integer contar) {
        // left 1 = (1)234
        if (contar > word.length()) contar = word.length();
        return word.substring(0, contar);
    }

    protected String getFromMap(Map<String, String> map, String toGet) {
        String retorno = null;
        if ((retorno = map.get(toGet)) == null) return "";
        return retorno;

    }

}
